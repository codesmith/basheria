using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace Basheria.Gfx
{
    public class Background : Microsoft.Xna.Framework.DrawableGameComponent
    {
        #region Constants
        const String BasePath = "Background\\";
        #endregion

        #region Attributes
        private List<Texture2D> layers;
        private SpriteBatch spriteBatch;
        #endregion 

        #region Constructors
        public Background(Game game)
            : base(game)
        {
            layers = new List<Texture2D>();
        }
        #endregion

        #region Methods from DrawableGameComponent
        public override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            Texture2D t = Game.Content.Load<Texture2D>(Background.BasePath + "desert");
            this.layers.Add(t);

            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend);
            foreach (Texture2D layer in this.layers)
            {
                Rectangle destRect = new Rectangle(0, 0, 
                    Game.GraphicsDevice.Viewport.Width, Game.GraphicsDevice.Viewport.Height);
                spriteBatch.Draw(layer, destRect, layer.Bounds, Color.White, 0.0f, new Vector2(0, 0), SpriteEffects.None, 0.9f);
            }
            spriteBatch.End();
        }
        #endregion
    }

/*
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class Background
    {
        #region Constants
        const String BasePath = "Background\\";
        #endregion

        #region Attributes
        private Game game;
        private List<Texture2D> layers;
        private ContentManager contentManager;
        #endregion

        #region Properties
        public SpriteBatch SpriteBatch { get; set; }
        #endregion

        public Background()
        {
            throw new InvalidOperationException();
        }

        public Background(Game game, ContentManager cm)
        {
            this.game = game;
            this.contentManager = cm;
            layers = new List<Texture2D>();
        }

        public void LoadBackground()
        {
            Texture2D t= this.contentManager.Load<Texture2D>(Background.BasePath + "desert");
            this.layers.Add(t);
        }

        public void Draw(GameTime gameTime)
        {
            foreach( Texture2D layer in this.layers)
            {
                Rectangle destRect = new Rectangle(0, 0, this.game.Window.ClientBounds.Width, this.game.Window.ClientBounds.Height);
                SpriteBatch.Draw(layer, destRect, layer.Bounds, Color.White, 0.0f, new Vector2(0,0), SpriteEffects.None, 0.9f);
            }
        }
    }
 */ 
}
