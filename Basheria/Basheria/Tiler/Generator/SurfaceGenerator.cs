﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using GameData;
using Basheria.Tiler;

namespace Basheria.WorldGenerator
{
    class SurfaceGenerator : WorldGeneratorBase
    {
        public SurfaceGenerator() : base()
        {
        }

        protected override void doGenerate()
        {
            int c = this.map.LayerCount;
            for (int i = 0; i < c; ++i)
            {
                generate(this.map.Layer(i));
            }
        }

        private void generate(MapLayer layer)
        {
            int cols = this.map.Width;
            int rows = this.map.Height;

            int startY = (int)(rows * 0.40f);
            int startX = 0;
            int limitY_top = (int)(rows * 0.30f);
            int limitY_bottom = (int)(rows * 0.50f);
            Random rand = new Random();
            int dir = 0;
            int r = 0;
            Tile tile = this.UsedTileSet.getTileByType( TileTypes.tile_surface );

            int posY = startY;
            int posYp = startY;
            int posYn = startY;

            //const int PositionPrev = 0;
            //const int PositionNow = 1;
            //const int PositionNext = 2;

            for (int i = 0; i < cols; i++, startX++)
            {
                posYp = posY;
                posY = posYn;
                
                r = rand.Next(100);

                if (r < 15) 
                    dir = -1;
                else if (r >= 85) 
                    dir = 1;
                else dir = 0;
                if (posY >= limitY_bottom) 
                {
                    dir = -1;
                }
                else if (posY <= limitY_top)
                {
                    dir = 1;
                }
                posYn += dir;
                tile = SelectTile(posY, posYp, posYn);
                layer.SetTileAt(startX, posY, tile);
                GenerateUnderSurface(layer, startX, posY + 1, 4 + rand.Next( 10 ));

            }
        }

        private void GenerateUnderSurface(MapLayer layer, int x, int y, int height)
        {
            for (int j = 0; j < height; j++)
            {
                Tile tile = this.UsedTileSet.getTileByType(TileTypes.tile_solid);
                layer.SetTileAt(x, y+j, tile);
            }
        }

        private Tile SelectTile(int posY, int posYp, int posYn)
        {
            Tile tile;
            if (posYp > posY && posYn > posY )
            {
                tile = this.UsedTileSet.getTileByType( TileTypes.tile_mound );
            }
            else if (posYp < posY && posYn < posY)
            {
                tile = this.UsedTileSet.getTileByType(TileTypes.tile_surface);
            }
            else if (posYn > posY && posYp < posY)
            {
                tile = this.UsedTileSet.getTileByType( TileTypes.tile_topright_corner );
            }
            else if (posYn < posY && posYp > posY)
            {
                tile = this.UsedTileSet.getTileByType( TileTypes.tile_topleft_corner );
            }
            else if (posYn > posY)
            {
                tile = this.UsedTileSet.getTileByType( TileTypes.tile_topright_corner );
            }
            else if (posYn == posY && posYp > posY)
            {
                tile = this.UsedTileSet.getTileByType(TileTypes.tile_topleft_corner);
            }
            else
            {
                tile = this.UsedTileSet.getTileByType(TileTypes.tile_surface);
            }
            return tile;
        }
    
    }
}
