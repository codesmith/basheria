using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using Basheria.Tiler;

namespace Basheria.Gfx
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class MapFrame : Microsoft.Xna.Framework.DrawableGameComponent
    {
        private SpriteBatch spriteBatch;

        public Camera Camera
        {
            get;
            set;
        }

        public GameMap Map
        {
            get;
            set;
        }

        public MapFrame(Game game, GameMap map)
            : base(game)
        {
            Map = map;
            Camera = new Camera();
            Camera.Position = new Vector2(0, 0);
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            base.LoadContent();
        }

        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend);

            int tileWidth = Map.GameTileSet.TileWidth;
            int tileHeight = Map.GameTileSet.TileHeight;

            int h = Game.GraphicsDevice.Viewport.Height / tileHeight;
            int w = Game.GraphicsDevice.Viewport.Width / tileWidth;

            int firstTileX = (int)(Camera.Position.X / tileWidth);
            int firstTileY = (int)(Camera.Position.Y / tileHeight);
            int layerCount = Map.LayerCount;
            Vector2 nullVector = new Vector2(0.0f, 0.0f);
            Rectangle destRect = new Rectangle(0, 0, tileWidth, tileHeight);
            for (int j = 0; j < h; j++)
            {
                for (int i = 0; i < w; i++)
                {
                    for (int k = 0; k < layerCount; ++k)
                    {
                        MapLayer layer = Map.Layer(k);
                        Tile tile = layer.TileAt(i + firstTileX, j + firstTileY);
                        if (tile != null)
                        {
                            destRect.X = i * tileWidth;
                            destRect.Y = j * tileHeight;
                            spriteBatch.Draw(tile.Texture, destRect, tile.SourceRectangle,
                                Color.White, 0.0f, nullVector, SpriteEffects.None, layer.Depth);
                        }
                    }
                }
            }
            spriteBatch.End(); 
            base.Draw(gameTime);
        }
    }
}
