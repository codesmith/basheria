﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework; // IGameComponent
using Microsoft.Xna.Framework.Content; // ContentManager
using Microsoft.Xna.Framework.Graphics; // Texture2D

using Basheria.Gfx;
// Custom gamedata (XML etc)
using GameData;

namespace Basheria.Tiler
{
    public class TileSet : IGameComponent
    {
        #region Constants
        private const String basePath = "TileGfx/";
        private Texture2D texture;
        #endregion

        #region Attributes
        private List<Tile> tileSet;
        #endregion

        #region Properties
        private TileSetData TileSetProperties
        {
            get;
            set;
        }

        private ContentManager Content
        {
            get;
            set;
        }

        public List<Tile> Tiles
        {
            get { return tileSet; }
        }

        public int TileWidth
        {
            get
            {
                int value = 0;
                if (TileSetProperties != null)
                {
                    value = TileSetProperties.PixelWidth;
                }
                return value;
            }
        }

        public int TileHeight
        {
            get
            {
                int value = 0;
                if (TileSetProperties != null)
                {
                    value = TileSetProperties.PixelHeight;
                }
                return value;
            }
        }


        #endregion

        #region Constructors
        public TileSet( ContentManager content )
        {
            this.Content = content;
            this.tileSet = null;
        }
        #endregion

        #region Methods from base classes
        public void Initialize()
        {
            // Load data for the tileset from XML
            this.TileSetProperties = Content.Load<TileSetData>("tileset");
            this.tileSet = new List<Tile>(this.TileSetProperties.TileDetail.Length);
            this.texture = this.Content.Load<Texture2D>(TileSetProperties.TileSetBitmapFile);

            for (int i = 0; i < this.TileSetProperties.TileDetail.Length; i++)
            {
                TileSetData.TileInfo entry = this.TileSetProperties.TileDetail[i];

                Rectangle rect = new Rectangle(0, 0, TileSetProperties.PixelWidth, TileSetProperties.PixelHeight);
                rect.X = entry.X * (TileSetProperties.PixelWidth + TileSetProperties.SpacingX);
                rect.Y = entry.Y * (TileSetProperties.PixelHeight + TileSetProperties.SpacingY);
                for (int j = 0; j < TileSetProperties.VariationsPerTile; j++)
                {
                 
                    Rectangle newRect = getTileRectangle(j, rect, this.TileSetProperties);
                    Tile tile = new Tile(texture, newRect, entry.TileType);
                    tileSet.Add(tile);
                }
            }
//            tileSet.Sort();
        }
        #endregion
        #region New methods
        private Rectangle getTileRectangle(int tileVariation, Rectangle baseRect, TileSetData prop)
        {
            Rectangle rect = new Rectangle();
            rect = baseRect;
            int newX = (baseRect.X + (tileVariation * (prop.PixelWidth + prop.SpacingX)));
            rect.X = newX % this.texture.Width;
            rect.Y = (newX / this.texture.Width > 0) ? ( rect.Y + prop.PixelHeight + prop.SpacingY ) : rect.Y;
            return rect;
        }

        public Tile getTileByType(int desiredType)
        {
            Tile newTile = null;
            int variation = new Random().Next(this.TileSetProperties.VariationsPerTile);
            for (int i = 0; i < tileSet.Count; ++i)
            {
                Tile t = tileSet[i];
                if (t.Type == desiredType)
                {
                    int tileIndex = tileSet.IndexOf(t);
                    tileIndex += variation;
                    newTile = tileSet[tileIndex];
                    break;
                }
            }

            return newTile;
        }
        #endregion
    }
}
