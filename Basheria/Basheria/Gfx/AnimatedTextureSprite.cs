﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Basheria.Gfx
{
    public class AnimatedTextureSprite : TextureSprite
    {
        #region Private attributes
        private int currentFrame;
        private float timePerFrame;
        private float totalElapsed;
        private int fpsValue;
        #endregion

        #region Properties
        public int FrameCount
        {
            get 
            {
                return Rows * Columns;
            }
        }
        public int Rows { get; set; }
        public int Columns { get; set; }

        public int Fps
        {
            set
            {
                this.timePerFrame = 1.0f / value;
                this.fpsValue = value;
            }
            get { return this.fpsValue; }
        }
        #endregion

        #region Constructors
        public AnimatedTextureSprite(Game game, String contentName, int fps)
            : base(game, contentName)
        {
            // Set default values to 1,1 so this will have only one tile
            this.Rows = 1;
            this.Columns = 1;
            // Animation starts from first frame
            this.currentFrame = 0;
            this.Fps = fps;
            this.Origin = new Vector2(0, 0); // Top left corner by default
        }
        
        public AnimatedTextureSprite(Game game, String contentName, Vector2 position)
            : this(game, contentName, 15)
        {
            this.Position = position;
        }
        #endregion

        #region Base class overrides
        public override void Draw(GameTime gameTime)
        {
            if (Texture != null)
            {
                int currentRow = (int)((float)this.currentFrame / (float)Columns);
                int currentColumn = this.currentFrame % Columns;
                Rectangle frameRect = FrameSize;
                frameRect.X = frameRect.Width * currentColumn;
                frameRect.Y = frameRect.Height * currentRow;
                spriteBatch.Begin();
                // @todo Origin is set at the moment to Top Left, this means rotation doesn't occur from center point currently
                spriteBatch.Draw(this.Texture, this.Position, frameRect,
                    this.tint, this.Rotation, this.Origin, this.Scale, SpriteEffects.None, this.Depth);
                spriteBatch.End();
            }
        }

        public override void Update(GameTime gameTime)
        {
            float elapsed = (float)gameTime.ElapsedGameTime.TotalSeconds;
            UpdateFrame(elapsed);
            
            base.Update(gameTime);
        }

        protected override void LoadContent()
        {
            // Load the given texture from ContentManager
            this.Texture = Game.Content.Load<Texture2D>(contentName);

            // Calculate the size of animation tile based on rows and columns
            this.frameSize = new Rectangle(0, 0, Texture.Width / Columns, Texture.Height / Rows);
        }
        #endregion

        #region Private class methods
        private void UpdateFrame(float elapsed)
        {
            totalElapsed += elapsed;
            if (totalElapsed > timePerFrame)
            {
                currentFrame++;
                // Keep the Frame between 0 and the total frames, minus one.
                currentFrame = currentFrame % FrameCount;
                totalElapsed -= timePerFrame;
            }
        }
        #endregion
    }
}
