using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace GameData
{
    // Constants for tiletypes
    public static class TileTypes
    {
        public static int empty = 0;
        public static int tile_solid = 1;
        public static int tile_topleft_corner = 2;
        public static int tile_surface = 3;
        public static int tile_topright_corner = 4;
        public static int tile_mound = 5; // 
        public static int tile_hollow = 6;
        public static int tile_left_straight = 7;
        public static int tile_right_straight = 8;
        public static int tile_surface_down = 9;
        public static int tile_bottomleft_corner = 10;
        public static int tile_bottomright_corner = 11;
    }
   
    public class TileSetData
    {
        public String TileSetBitmapFile; 
        public int Columns;
        public int Rows;
//        public int TileCount;        
        public int PixelWidth;
        public int PixelHeight;
        public int SpacingX;
        public int SpacingY;
        public int VariationsPerTile;
        public class TileInfo
        {
            // Defines which type of tile it is, e.g. left up corner, solid ground etc.
            public int TileType;
            // Defines the position of the tile within the bitmap horizontally
            public int X;
            // Defines the position of the tile within the bitmap vertically
            public int Y;

        }
        public TileInfo[] TileDetail;
    }
}
