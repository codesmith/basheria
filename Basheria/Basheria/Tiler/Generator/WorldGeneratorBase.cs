﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Basheria.Tiler;

namespace Basheria.WorldGenerator
{
    /* <summary>
     * Base class for every generator that touches the Map
     * 
     * Concrete generators must be inherited from this
     * </summary>
     */
    abstract class WorldGeneratorBase
    {
        #region Attributes
        protected int iterations;
        protected int maxIterations;
        protected GameMap map;
        #endregion 

        #region Properties
        public TileSet UsedTileSet { get; set; }
        #endregion

        #region Constructors
        public WorldGeneratorBase()
            : this(0)
        {
        }

        public WorldGeneratorBase(int limit)
        {
            this.maxIterations = limit;
            iterations = 0;
        }
        #endregion

        public void Generate(GameMap gameMap)
        {
            this.map = gameMap;
            this.iterations = 0;
            doGenerate();
        }

        // Must be overridden
        protected abstract void doGenerate();
    }
}
