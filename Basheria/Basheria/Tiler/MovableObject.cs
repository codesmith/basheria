﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

using Basheria.Gfx;

namespace Basheria.Tiler
{
    public class MovableObjectEventArgs : EventArgs
    {
        public Vector2 Diff;
        public Vector2 Position;
    }

    public delegate void PositionChangedHandler(object sender, MovableObjectEventArgs e);

    public class MovableObject
    {
        #region Delegates
        #endregion

        #region Properties
        public event PositionChangedHandler PositionChanged;

        public Vector2 Position
        {
            get;
            set;
        }
        #endregion

        #region Constructors
        public MovableObject()
        {
            this.Position = new Vector2(0, 0);
        }

        public MovableObject(Vector2 position)
        {
            this.Position = position;
        }
        #endregion

        protected virtual void OnPositionChanged(MovableObjectEventArgs e)
        {
            if (this.PositionChanged != null)
            {
                this.PositionChanged(this, e);
            }
        }

        #region New methods
        public void Move(Vector2 amount)
        {
            this.Position += amount;
            MovableObjectEventArgs args = new MovableObjectEventArgs();
            args.Diff = amount;
            args.Position = this.Position;
            
            this.OnPositionChanged(args);            
        }
        #endregion
    }
}
