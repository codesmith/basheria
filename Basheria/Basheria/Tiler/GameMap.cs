﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

using Basheria.WorldGenerator;

namespace Basheria.Tiler
{    
    public class GameMap
    {
        #region Attributes
        private TileSet tileSet;
        private List<MapLayer> layers;
        #endregion

        #region Properties
        public int Height 
        { 
            get; 
            set; 
        }
        public int Width 
        { 
            get; 
            set; 
        }

        public int LayerCount
        {
            get
            {
                Debug.Assert(this.layers != null, "Layers not yet added to the world map");
                return this.layers.Count;
            }
        }

        public TileSet GameTileSet
        {
            get { return tileSet; }
            set { tileSet = value; }
        }
 
        #endregion

        #region Constructors
        public GameMap() : this(0,0)
        {
        }

        public GameMap(int width, int height)
        {
            this.layers = new List<MapLayer>();
            this.Width = width;
            this.Height = height;
        }
        #endregion

        #region New methods
        public void Clear()
        {
            foreach (MapLayer layer in this.layers)
            {
                layer.Clear();
            }
        }

        public void AddLayer(String name, float order )
        {
            MapLayer layer = new MapLayer(this.Width, this.Height, name);
            layer.Depth = order;
            this.layers.Add(layer);
        }

        public MapLayer Layer(int num)
        {
            Debug.Assert(this.layers != null, "GameMap layers is not set (null)");
            Debug.Assert(num >= 0, "Trying to access negative layer number");
            return this.layers[num];
        }

        public void Generate()
        {
            SurfaceGenerator surface = new SurfaceGenerator();
            surface.UsedTileSet = this.tileSet;
            surface.Generate(this);
        }

        #endregion

    }
}
