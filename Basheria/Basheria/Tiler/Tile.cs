﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using Basheria.Gfx;
using GameData;

namespace Basheria.Tiler
{
    public class Tile : IComparable
    {
        public Texture2D Texture { get; set; }
        public Rectangle SourceRectangle { get; set; }
        public int Type { get; set; }

        public Tile(Texture2D texture, Rectangle rect, int type )
        {
            this.Texture = texture;
            this.SourceRectangle = rect;
            this.Type = type;
        }

        public int CompareTo(object obj)
        {
            Tile t = obj as Tile;
            return this.Type - t.Type;
        }
    }
}
