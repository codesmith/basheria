using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using Basheria.WorldGenerator;
using Basheria.Gfx;
using Basheria.Tiler;
using Basheria.UI;
using GameData;

namespace Basheria
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class BasheriaGame : Microsoft.Xna.Framework.Game
    {
        #region Constants
        private const int gameScreenWidth = 800;
        private const int gameScreenHeight =600;
        #endregion

        #region Attributes
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        private GameMap map;
        private TileSet tileSet;
        private MapFrame mapFrame;
        private Background backgroundLayers;
        private UIFrame uiFrame;
        #endregion

        #region Constructors
        public BasheriaGame()
        {
            graphics = new GraphicsDeviceManager(this);

            graphics.PreferredBackBufferWidth = BasheriaGame.gameScreenWidth;
            graphics.PreferredBackBufferHeight = BasheriaGame.gameScreenHeight;
            graphics.IsFullScreen = false;

            Content.RootDirectory = "Content";
            map = new GameMap(1024, 128);

            TargetElapsedTime = TimeSpan.FromSeconds(1 / 30.0);
        }
        #endregion

        #region Overridden methods
        protected override void Initialize()
        {
            tileSet = new TileSet(this.Content);
            Components.Add(tileSet);

            backgroundLayers = new Background(this);
            Components.Add(backgroundLayers);

            mapFrame = new MapFrame(this, this.map);
            Components.Add(mapFrame);

            uiFrame = new UIFrame(this);
            uiFrame.Camera = mapFrame.Camera;
            Components.Add(uiFrame);

            base.Initialize();

        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            this.map.GameTileSet = tileSet;
            this.map.AddLayer("Test", 0.1f);
            //this.map.AddLayer("Another test", 0.2f);
            this.map.Generate();

            
            base.LoadContent();
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            HandleGamePad(GamePad.GetState(PlayerIndex.One));
            HandleKeyboard(Keyboard.GetState(PlayerIndex.One));
            HandleMouse(Mouse.GetState());
            // update animations
//            foreach( Tile tile in this.tileSet.Tiles )
//            {
//                tile.Texture.UpdateBlock(gameTime);
//            }

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            base.Draw(gameTime);

        }
        #endregion

        #region New methods
        private void HandleKeyboard(KeyboardState keyboardState)
        {
            int scrollOffset = 32;
            float x = 0;
            float y = 0;
            if (keyboardState.IsKeyDown(Keys.G) )
            {
                map.Clear();
                map.Generate();
            } 
            else if( keyboardState.IsKeyDown(Keys.Right))
            {
                x = scrollOffset;
            }
            else if (keyboardState.IsKeyDown(Keys.Left))
            {
                if (mapFrame.Camera.Position.X - scrollOffset >= 0)
                {
                    x = -scrollOffset;
                }
            }
            else if (keyboardState.IsKeyDown(Keys.Down))
            {
                y = scrollOffset;
            }
            else if (keyboardState.IsKeyDown(Keys.Up))
            {
                if (mapFrame.Camera.Position.Y - scrollOffset >= 0)
                {
                    y =- scrollOffset;
                }
            }
            else if (keyboardState.IsKeyDown(Keys.Escape))
            {
                this.Exit();
            }
            if (x != 0 || y != 0)
            {
                mapFrame.Camera.Move(new Vector2(x, y));
            }
        }

        private void HandleMouse( MouseState mouseState)
        {
        }

        private void HandleGamePad(GamePadState gamePadState)
        {
            if (gamePadState.Buttons.Back == ButtonState.Pressed)
            {
                this.Exit();
            }
        }
        #endregion

    }
}
