﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace Basheria.Tiler
{
    //
    // TileRow - One row of tiles in a Map layer
    //
    public class TileRow
    {
        public List<Tile> Columns;
        public TileRow(int columns)
        {
            this.Columns = new List<Tile>(columns);
            for (int i = 0; i < columns; i++)
            {
                this.Columns.Add(null);
            }
        }

        public TileRow()
            : this(0)
        {
        }
    }

    public class MapLayer
    {
        #region Attributes
        List<TileRow> rows;
        #endregion 

        #region Properties
        public int Width
        {
            get;
            set;
        }

        public int Height
        {
            get;
            set;
        }

        public String LayerName
        {
            get;
            set;
        }

        public float Depth
        {
            get;
            set;
        }
        #endregion

        #region Constructors
        public MapLayer()
            : this(0, 0)
        {
        }

        public MapLayer(int width, int height, String layerName = "default")
        {
            this.Width = width;
            this.Height = height;
            this.LayerName = layerName;
            this.Depth = 0;
            this.rows = new List<TileRow>();
            for (int i = 0; i < this.Height; i++)
            {
                this.rows.Add(new TileRow(this.Width));
            }
        }
        #endregion

        #region New methods
        public void Clear()
        {
            foreach (TileRow row in this.rows)
            {
                for (int i = 0; i < this.Width; i++)
                {
                    row.Columns[i] = null;
                }
            }
        }

        public Tile TileAt(int x, int y)
        {
            Debug.Assert(x >= 0 && x < this.Width);
            Debug.Assert(y >= 0 && y < this.Height);
            return this.rows[y].Columns[x];
        }

        public void SetTileAt(int x, int y, Tile tile)
        {
            Debug.Assert(x >= 0 && x < this.Width);
            Debug.Assert(y >= 0 && y < this.Height);
            this.rows[y].Columns[x]=tile;
        }

        #endregion
    }
}
