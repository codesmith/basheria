using System;

namespace Basheria
{
#if WINDOWS || XBOX
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            using (BasheriaGame game = new BasheriaGame())
            {
                game.Run();
            }
        }
    }
#endif
}

