using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace Basheria.Gfx
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class TextureSprite : Microsoft.Xna.Framework.DrawableGameComponent
    {
        #region Private and protected attributes
        protected Rectangle frameSize;
        protected String contentName;
        #endregion

        #region Setters and Getter Properties
        public Texture2D Texture { get; set; }
        public Vector2 Position { get; set; }
        public SpriteBatch spriteBatch { get; set; }
        public Color tint { get; set; }
        public Vector2 Origin { get; set; }
        public float Depth { get; set; }
        public float Scale { get; set; }
        public float Rotation { get; set; }
        public Rectangle FrameSize
        {
            get { return this.frameSize; }
        }

        public float TargetWidth
        {
            set
            {
                this.Scale = value / FrameSize.Width;
            }
        }
        #endregion

        #region Constructors
        public TextureSprite(Game game, String contentName)
            : base(game)
        {
            this.Rotation = 0.0f;
            this.Position = new Vector2(0.0f, 0.0f);
            this.contentName = contentName;
            this.spriteBatch = null;
            this.tint = Color.White;
            this.Scale = 1.0f;
            frameSize = new Rectangle(0, 0, 0, 0);
        }

        public TextureSprite(Game game, String contentName, Vector2 position)
            : this(game, contentName)
        {
            this.Position = position;
        }
        #endregion

        #region Base class overrides
        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            // TODO: Add your initialization code here

            base.Initialize();
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        //
        // Summary:
        //     Called when the DrawableGameComponent needs to be drawn. Override this method
        //     with component-specific drawing code. Reference page contains links to related
        //     conceptual articles.
        //
        // Parameters:
        //   gameTime:
        //     Time passed since the last call to Draw.
        public override void Draw(GameTime gameTime)
        {
            if (Texture != null)
            {
                spriteBatch.Begin();
                spriteBatch.Draw(this.Texture, this.Position,
                    null, this.tint, this.Rotation, this.Origin, this.Scale, SpriteEffects.None, this.Depth);
                spriteBatch.End();
            }
        }
 
        protected override void LoadContent()
        {
            base.LoadContent();
            
            // Load the given texture from ContentManager
            this.Texture = Game.Content.Load<Texture2D>(contentName);
            frameSize = Texture.Bounds;
            Origin = new Vector2(frameSize.Width / 2.0f, frameSize.Height / 2.0f);
        }
        #endregion
    }
}
