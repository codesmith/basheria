using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using Basheria.Tiler;

namespace Basheria.UI
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class UIFrame : Microsoft.Xna.Framework.DrawableGameComponent
    {
        #region Attributes
        Vector2 fontPos;
        SpriteBatch spriteBatch;
        Vector2 cameraPos;
        #endregion

        #region Properties
        private SpriteFont StatusFont
        {
            get;
            set;
        }

        public Camera Camera
        {
            get;
            set;
        }
        #endregion

        #region Event Listeners
        private void CameraPositionChanged(object sender, MovableObjectEventArgs e)
        {
            cameraPos = e.Position;
        }
        #endregion

        #region Constructors
        public UIFrame(Game game)
            : base(game)
        {
            cameraPos = Vector2.Zero;
        }
        #endregion

        #region Methods from DrawableGameComponent
        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            // TODO: Add your initialization code here
            if (Camera != null)
            {
                Camera.PositionChanged += new PositionChangedHandler(this.CameraPositionChanged);
            }
            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            StatusFont = Game.Content.Load<SpriteFont>("StatusFont");
            fontPos = new Vector2(Game.GraphicsDevice.Viewport.Width / 2, StatusFont.LineSpacing );
            base.LoadContent();
        }
        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend);

            // Draw Hello World
            string output = "Hello World";
            string position = "Camera @ " + (int)cameraPos.X + ":" + (int)cameraPos.Y;

            Vector2 line = fontPos;

            // Find the center of the string
            Vector2 fontOrigin = StatusFont.MeasureString(output) / 2;
            // Draw the string
            spriteBatch.DrawString(StatusFont, output, line, Color.Black,
                0, fontOrigin, 1.0f, SpriteEffects.None, 0.0f);
            line.Y += StatusFont.LineSpacing;
            fontOrigin = StatusFont.MeasureString(position) / 2;
            spriteBatch.DrawString(StatusFont, position, line, Color.Black,
                0, fontOrigin, 1.0f, SpriteEffects.None, 0.0f);
            spriteBatch.End();
            base.Draw(gameTime);
        }
        #endregion
    }
}
