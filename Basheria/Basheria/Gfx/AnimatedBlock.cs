﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace Basheria.Gfx
{
    public class AnimatedBlock
    {
        #region Private attributes
        private int currentFrame;
        private float timePerFrame;
        private float totalElapsed;
        private int fpsValue;
        private Vector2 nullVector = new Vector2(0.0f, 0.0f);
        private Rectangle frameSize;
        #endregion

        #region Properties
        public Texture2D Texture 
        { 
            get; 
            set; 
        }

        public int FrameCount
        {
            get
            {
                return Rows * Columns;
            }
        }

        public int Rows 
        { 
            get; 
            set; 
        }

        public int Columns 
        { 
            get; 
            set; 
        }

        public int Fps
        {
            set
            {
                this.timePerFrame = 1.0f / value;
                this.fpsValue = value;
            }
            get { return this.fpsValue; }
        }

        public Rectangle FrameSize
        {
            get 
            { 
                return this.frameSize; 
            }
        }
        #endregion

        #region Contructors
        public AnimatedBlock(int colums, int rows, int fps)
        {
            Columns = colums;
            Rows = rows;
            Fps = fps;
            currentFrame = 0;
        }
        #endregion

        #region New methods
        public void LoadTexture(String assetName, ContentManager content)
        {
            Texture = content.Load<Texture2D>(assetName);
            this.frameSize = new Rectangle(0, 0, Texture.Width / Columns, Texture.Height / Rows);
        }

        public void DrawBlock( SpriteBatch spriteBatch, Vector2 position, Color color )
        {
            if (Texture != null)
            {
                int currentRow = (int)((float)this.currentFrame / (float)Columns);
                int currentColumn = this.currentFrame % Columns;
                Rectangle frameRect = FrameSize;
                frameRect.X = frameRect.Width * currentColumn;
                frameRect.Y = frameRect.Height * currentRow;
                
                spriteBatch.Draw(this.Texture, position, frameRect,
                    color, 0.0f, nullVector, 1.0f, SpriteEffects.None, 1);
            }
        }

        public void UpdateBlock(GameTime gameTime)
        {
            float elapsed = (float)gameTime.ElapsedGameTime.TotalSeconds;
            totalElapsed += elapsed;

            if (FrameCount > 1 )
            {
                if (totalElapsed > timePerFrame)
                {
                    currentFrame++;
                    currentFrame = currentFrame % FrameCount;
                    totalElapsed -= timePerFrame;
                }
            }
        }           
        #endregion
    }
}
